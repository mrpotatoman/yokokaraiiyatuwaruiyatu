﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaruiyatuScript : MonoBehaviour
{
    
    public float waruiyatuspeed = 0.1f;
    GameObject time;

    // Start is called before the first frame update
    void Start()
    {
        time = GameObject.Find("PAUSE");
    }

    // Update is called once per frame
    void Update()
    {
        if (time.GetComponent<TimeKeeper>().isPose == false)
        {
            transform.Translate(0f, waruiyatuspeed * Time.deltaTime, 0f);

            if (transform.position.y < -10)
            {
                Destroy(gameObject);
            }
        }
            
    }
}

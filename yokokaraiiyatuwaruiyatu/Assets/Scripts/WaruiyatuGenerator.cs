﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaruiyatuGenerator : MonoBehaviour
{
    public GameObject wr;     //ゲームオブジェクトを扱えるように箱を作っておく
    public float span2 = 3.0f;   //次にアイスが生成されるまでのスパンタイム
    float delta = 0;    //最初は0からスタートさせる
    GameObject time;

    // Start is called before the first frame update
    void Start()
    {
        time = GameObject.Find("PAUSE");
    }

    // Update is called once per frame
    void Update()
    {
        if (time.GetComponent<TimeKeeper>().isPose == false)
        {
            //deltaって箱にさっきのUpdateから経った時間を入れていく(足していく)
            delta += Time.deltaTime;
            //もしspanって量を超えたら
            if (delta > span2)
            {
                //まず箱を空にする
                delta = 0;


                for (int a = 0; a < 2; a++)
                {
                    //次にIce1ってオブジェクトを生成する
                    GameObject kansei2 = Instantiate(wr) as GameObject;
                    //作ったオブジェクトのx座標を先にランダムで決めておく
                    float py = Random.Range(-3.4f, 3.4f);
                    //作ったオブジェクトをこの場所に移動させる
                    kansei2.transform.position = new Vector3(10, py, 0);
                }

            }
        }
            
    }
}

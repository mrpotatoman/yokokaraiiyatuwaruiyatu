﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonballScript : MonoBehaviour
{
    public float idousokudocannon;

    GameObject czscore;

    GameObject plcon;

    GameObject hpjewel;

    GameObject time;
    
    // Start is called before the first frame update
    void Start()
    {
        czscore = GameObject.Find("ScoreText");
        plcon = GameObject.Find("Cube");
        hpjewel = GameObject.Find("RedJewel");
        time = GameObject.Find("PAUSE");
    }

    // Update is called once per frame
    void Update()
    {
        if(time.GetComponent<TimeKeeper>().isPose == false)
        {
            transform.Translate(idousokudocannon * Time.deltaTime, 0f, 0f);


            if (transform.position.x > 12)
            {
                Destroy(gameObject);
            }
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Iiyatu")
        {
            if(hpjewel.GetComponent<HpScript>().deathblow == false)
            {
                Destroy(other.gameObject);
                czscore.GetComponent<CrazyScore>().sendscore += 200;
            }
            else
            {
                Destroy(other.gameObject);
                czscore.GetComponent<CrazyScore>().sendscore += 1500;
            }
            
        }


        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpScript : MonoBehaviour
{
    public float hp = 0;

    public bool deathblow = false;
    GameObject time;

    // Start is called before the first frame update
    void Start()
    {
        time = GameObject.Find("PAUSE");
    }

    // Update is called once per frame
    void Update()
    {
        if (time.GetComponent<TimeKeeper>().isPose == false)
        {
            GetComponent<Image>().fillAmount = hp;

            if (hp > 1)
            {
                hp = 1;
            }

            if (hp >= 1)
            {
                deathblow = true;
            }

            if (hp <= 0)
            {
                deathblow = false;
            }

            if (deathblow == true)
            {
                if (hp > 0)
                {
                    hp -= 0.003f;
                }
            }
        }
            
        
    }
}

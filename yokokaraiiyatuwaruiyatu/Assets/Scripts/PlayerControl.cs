﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{
    public float yup = 0.03f;
    public float ydown = -0.03f;
    GameObject stx;

    GameObject hpj;

    public GameObject audio1;

    public GameObject audio2;

    public GameObject specialcannon;
    

    public GameObject cannonball;
    GameObject time;

    // Start is called before the first frame update
    void Start()
    {
        stx = GameObject.Find("ScoreText");
        hpj = GameObject.Find("RedJewel");
        audio1 = GameObject.Find("Canvas");
        audio2 = GameObject.Find("DirectionalLight");
        time = GameObject.Find("PAUSE");

    }

    // Update is called once per frame
    void Update()
    {

        if (time.GetComponent<TimeKeeper>().isPose == false)
        {
            if (transform.position.y < 5.4f)
            {
                if (transform.position.y > -3.2f)
                {
                    if (Input.GetKey(KeyCode.UpArrow))
                    {
                        transform.Translate(0f, yup * Time.deltaTime, 0f);
                    }
                    if (Input.GetKey(KeyCode.DownArrow))
                    {
                        transform.Translate(0f, ydown * Time.deltaTime, 0f);
                    }

                }
                else
                {
                    if (Input.GetKey(KeyCode.UpArrow))
                    {
                        transform.Translate(0f, yup * Time.deltaTime, 0f);
                    }
                }

            }
            else
            {
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    transform.Translate(0f, ydown * Time.deltaTime, 0f);
                }
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (hpj.GetComponent<HpScript>().deathblow == false)
                {
                    GameObject cannon = Instantiate(cannonball);
                    float koko = transform.position.y;
                    cannon.transform.Translate(-4.5f, koko, 0f);
                }
                else
                {
                    GameObject special = Instantiate(specialcannon);
                    float koko = transform.position.y;
                    special.transform.Translate(-7.5f, koko, 0f);
                }

            }
        }
            


    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Iiyatu")
        {
            audio1.GetComponent<Audio1>().niceflag = true;

            stx.GetComponent<CrazyScore>().sendscore += 50;

            Destroy(other.gameObject);
        }
        else if(other.gameObject.tag == "Waruiyatu")
        {
            audio2.GetComponent<Audio2>().badflag = true;

            if (hpj.GetComponent<HpScript>().deathblow == false)
            {
                stx.GetComponent<CrazyScore>().sendscore -= 10000;
                hpj.GetComponent<HpScript>().hp += 0.2f;
                Destroy(other.gameObject);
            }
            else
            {
                Destroy(other.gameObject);
            }
            
        }
    }
}

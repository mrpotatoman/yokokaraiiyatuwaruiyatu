﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResultScript : MonoBehaviour
{

    int finalscore;
    int finalbest;

    GameObject besttext;

    // Start is called before the first frame update
    void Start()
    {
        finalscore = CrazyScore.totalscore();
        finalbest = CrazyScore.sendBestScore();
        besttext = GameObject.Find("BestScoreResult");

        if (finalscore > 0)
        {
            GetComponent<Text>().text = finalscore + "京点";
        }
        else
        {
            GetComponent<Text>().text = "エア京";
        }

        if (finalbest > 0)
        {
            besttext.GetComponent<Text>().text = finalscore + "京点";
        }
        else
        {
            besttext.GetComponent<Text>().text = "エア京";
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene("PlayScene");
        }
    }
}

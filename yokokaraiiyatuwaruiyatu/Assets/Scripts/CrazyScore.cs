﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrazyScore : MonoBehaviour
{

    GameObject ScoreText;



    public int sendscore;

    static public int oneTimeInt;


    static public int totalscore()
    {
        return oneTimeInt;
    }


    private int bestScore;  //ハイスコア用変数

    static public int oneTimeBestInt;

    static public int sendBestScore()
    {
        return oneTimeBestInt;
    }


    private string key = "BEST SCORE";  //ハイスコアの保存先キー



    // Start is called before the first frame update
    void Start()
    {

        ScoreText = GameObject.Find("ScoreText");



        //保存しておいたハイスコアをキーで呼び出し取得し保存されていなければ0になる
        bestScore = PlayerPrefs.GetInt(key, 0);


        //ハイスコアを表示(小数点以下第一位まで)
        ScoreText.GetComponent<Text>().text = "BEST: " + bestScore + "京";
    }

    // Update is called once per frame
    void Update()
    {

        oneTimeInt = sendscore;
        

        if(sendscore > 0)
        {
            ScoreText.GetComponent<Text>().text = "スコア:" + sendscore + "京";
        }
        else
        {
            sendscore = 0;
            ScoreText.GetComponent<Text>().text = "スコア:エア京";
        }
        


        //ハイスコアより現在スコアが高い時
        if (sendscore > bestScore)
        {
            
            //ハイスコア更新
            bestScore = sendscore;


            //ハイスコアを保存
            PlayerPrefs.SetInt(key, bestScore);


            //ハイスコアを表示(小数点以下第一位まで)
            ScoreText.GetComponent<Text>().text = "BEST: " + bestScore + "京";
        }

        oneTimeBestInt = bestScore;
    }
}

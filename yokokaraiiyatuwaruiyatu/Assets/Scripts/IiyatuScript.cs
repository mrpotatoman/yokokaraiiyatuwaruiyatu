﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IiyatuScript : MonoBehaviour
{

    public Material heart;
    public Material clover;
    public Material spade;
    public float iiyatuspeed = -0.1f;
    GameObject time;


    // Start is called before the first frame update
    void Start()
    {
        int num = Random.Range(0, 3);

        if (num == 0)
        {
            this.GetComponent<Renderer>().material = heart;
        }
        if (num == 1)
        {
            this.GetComponent<Renderer>().material = clover;
        }
        if (num == 2)
        {
            this.GetComponent<Renderer>().material = spade;
        }
        time = GameObject.Find("PAUSE");
    }

    // Update is called once per frame
    void Update()
    {
        if (time.GetComponent<TimeKeeper>().isPose == false)
        {
            transform.Translate(iiyatuspeed * Time.deltaTime, 0f, 0f);

            if (transform.position.x < -10)
            {
                Destroy(gameObject);
            }
        }
        
    }
}

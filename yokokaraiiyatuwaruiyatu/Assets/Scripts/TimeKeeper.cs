﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimeKeeper : MonoBehaviour
{
    //カウントダウン
    public float countdown = 10.0f;

    //時間を表示するText型の変数
    public Text timeText;

    //ポーズしているかどうか
    public bool isPose = false;

    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        //クリックされたとき
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //ポーズ中にクリックされたとき
            if (isPose)
            {
                //ポーズ状態を解除する
                isPose = false;
            }
            //進行中にクリックされたとき
            else
            {
                //ポーズ状態にする
                isPose = true;
            }
        }

        //ポーズ中かどうか
        if (isPose)
        {
            //ポーズ中であることを表示
            timeText.text = "PAUSE";

            //カウントダウンしない
            return;
        }

        //時間をカウントする
        countdown -= Time.deltaTime;

        //時間を表示する
        timeText.text = countdown.ToString("f1") + "秒";

        //countdownが0以下になったとき
        if (countdown <= 0)
        {
            timeText.text = "FINISH";
            SceneManager.LoadScene("resultscene");
        }
    }
}
